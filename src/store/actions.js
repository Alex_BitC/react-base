import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';


const deleteMessage = createAction(ActionType.DELETE_MESSAGE, (id) => ({
  payload: {
    id
  }
}));

const addMessage = createAction(ActionType.ADD_MESSAGE, (message) => ({
  payload: {
    message
  }
}));

const setCurrentUser = createAction(ActionType.SET_CURRENT_USER, (login, password) => ({
  payload: {
    login,
    password
  }
}));

const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async (_args, { extra }) => ({
    messages: await extra.http.load('https://edikdolynskyi.github.io/react_sources/messages.json'),
}));

async (userId, thunkAPI) => {
  const response = await userAPI.fetchById(userId)
  return response.data
}

export {
  deleteMessage,
  addMessage,
  setCurrentUser,
  fetchMessages
};
