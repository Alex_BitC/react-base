import { configureStore } from '@reduxjs/toolkit';
import {Rootreducer} from './root-reducer';
import {http} from '../services/services'

const store = configureStore({
  reducer: Rootreducer,
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      thunk: {
        extraArgument: {
          http,
        },
      },
      serializableCheck: false,
    });
  },
});

export default store;
