const ActionType = {
  FETCH_MESSAGES: 'chat/fetch-messages',
  ADD_MESSAGE: 'chat/add-message',
  UPDATE_MESSAGE: 'chat/update-message',
  DELETE_MESSAGE: 'chat/delete-message',
  FETCH_USERS: 'users/fetch-users',
  ADD_USER: 'users/add-user',
  UPDATE_USER: 'users/update-user',
  DELETE_MESSAGE: 'users/delete-user',
  SET_CURRENT_USER: 'users/set-current-user',
};

export { ActionType };
