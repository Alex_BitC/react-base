import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import { deleteMessage, addMessage, setCurrentUser, fetchMessages } from './actions';
import {UsersFromJSON } from '../users';

const initialState = {
  chat: {
    messages: []
  },
  participants: [],
  dateLastMessage: null,
  users: [],
  editModal: false,
  preloader: true,
  admin: null,
  currentUser: null
};

const Rootreducer = createReducer(initialState, builder => {
  builder.addCase(deleteMessage, (state, { payload }) => {
    const { id } = payload;
    state.chat.messages = state.chat.messages.filter(message => message.id !== id);
    state.dateLastMessage = state.chat.messages[0].createdAt;

    state.participants =  Array.from(new Set(state.chat.messages.map(message => message.userId)));

    state.preloader = false;
  });
  builder.addCase(addMessage, (state, { payload }) => {
    const { message } = payload;
    state.chat.messages = [message, ...state.chat.messages];
    state.dateLastMessage = state.chat.messages[0].createdAt;

    state.participants =  Array.from(new Set(state.chat.messages.map(message => message.userId)));

    state.preloader = false;
  });
  builder.addCase(setCurrentUser, (state, { payload }) => {
    const { login, password } = payload;
    const currentUser = UsersFromJSON.find(user => user.login === login && user.password === password);
    if (currentUser) {
      state.currentUser = currentUser;
      if (currentUser.login === 'admin' && currentUser.password === 'admin') {
        state.admin = currentUser;
      }
    }
  });
  builder.addCase(fetchMessages.pending, (state) => {
    state.preloader = true;
  });
  builder.addCase(fetchMessages.fulfilled, (state, { payload }) => {
    const { messages } = payload;

    state.chat.messages = messages;

    state.dateLastMessage = state.chat.messages[0].createdAt;

    state.participants =  Array.from(new Set(state.chat.messages.map(message => message.userId)));

    state.preloader = false;

  });

  // builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
  //   const { posts } = action.payload;

  //   state.posts = posts;
  //   state.hasMorePosts = Boolean(posts.length);
  // });
  // builder.addCase(threadActions.loadMorePosts.pending, state => {
  //   state.hasMorePosts = null;
  // });
  // builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
  //   const { posts } = action.payload;

  //   state.posts = state.posts.concat(posts);
  //   state.hasMorePosts = Boolean(posts.length);
  // });
  // builder.addCase(threadActions.toggleExpandedPost.fulfilled, (state, action) => {
  //   const { post } = action.payload;

  //   state.expandedPost = post;
  // });
  // builder.addMatcher(isAnyOf(threadActions.likePost.fulfilled, threadActions.addComment.fulfilled), (state, action) => {
  //   const { posts, expandedPost } = action.payload;
  //   state.posts = posts;
  //   state.expandedPost = expandedPost;
  // });
  // builder.addMatcher(isAnyOf(
  //   threadActions.applyPost.fulfilled,
  //   threadActions.createPost.fulfilled
  // ), (state, action) => {
  //   const { post } = action.payload;

  //   state.posts = [post, ...state.posts];
  // });
});

export { Rootreducer };
