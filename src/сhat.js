import { Route, Routes } from 'react-router-dom';
import React, { useEffect } from 'react';
import { AppRoute } from './components/common/enums/app-route';
import PublicRoute from './components/common/public-route/public-route';
import PrivateRoute from './components/common/private-route/private-route';
import ProtectedRoute from './components/common/protected-route/protected-route';
import LoginPage from './components/login-page/login-page';
import ChatdPage from './components/chat-page/chat-page';
import UsersPage from './components/users-page/users-page';
import UserEditPage from './components/user-edit-page/user-edit-page';
import NotFoundPage from './components/not-found-page/not-found-page';

// import Preloader from './components/common/preloader/preloader';
// import Header from './components/header/header';

function Сhat() {
  return (
    <Routes>
      <Route path={AppRoute.LOGIN} element={<PublicRoute component={LoginPage} />} />
      <Route path={AppRoute.ROOT} element={<PrivateRoute component={ChatdPage} />} />
      <Route path={AppRoute.USERS} element={<ProtectedRoute component={UsersPage} />} />
      <Route path={AppRoute.USERS_EDIT_$ID} element={<ProtectedRoute component={UserEditPage} />} />
      <Route path={AppRoute.ANY} element={<NotFoundPage />} />
    </Routes>
  );
}

export default Сhat;
