function convertLeadZero(number) {
  return (number <= 9 ? '0' + number : number)
}

export const formatDateToDD_MM_YYYY_HH_MM_SS = (dateString) => {
  const date = new Date(dateString)
    const day = date.getDate();
    const month = date.getMonth() + 1; //Month from 0 to 11
    const year = date.getFullYear();
    const hour = date.getHours();
    const minite = date.getMinutes();
    const seconds = date.getSeconds();

    return '' + convertLeadZero(day) + '.'
              + convertLeadZero(month) + '.'
              + year + ' '
              + convertLeadZero(hour) + ':'
              + convertLeadZero(minite) + ':'
              + convertLeadZero(seconds);
}

export const formatDateToHH_MM = (dateString) => {
  const date = new Date(dateString);
    const hour = date.getHours();
    const minite = date.getMinutes();

    return '' + convertLeadZero(hour) + ':'
              + convertLeadZero(minite);
}

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

// Monday, 17 June


export const createTimeStamp = () => {
  const date = new Date();
  return date.toISOString();
}

const setDivider = (messageDate) => {

  const today = new Date();
  today.setHours(0,0,0,0);


  const yesterday = new Date(today);
  yesterday.setDate(yesterday.getDate() - 1);
  if (messageDate >= today) {
    return 'Today';
  }

  if  (messageDate.valueOf() >= yesterday.valueOf()  && messageDate.valueOf() < today.valueOf() ) {
    return 'Yesterday';
  }
  const dayMonth = messageDate.getDate();
  const dayWeek = messageDate.getDay();
  const month = messageDate.getMonth()
  return `${days[dayWeek]}, ${convertLeadZero(dayMonth)} ${months[month]}`
}


export const addDividers = (messages) => {
  let currentDate;
  const result = [];
  messages.forEach(message => {
    const messageDate = new Date(message.createdAt);
    if (!currentDate || messageDate < currentDate) {
      currentDate = messageDate.setHours(0,0,0);
      const divider = setDivider(messageDate);
      result.push(divider);
    }
    result.push(message);
  });
  return result;
}
