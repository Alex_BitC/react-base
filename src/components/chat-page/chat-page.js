import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteMessage, addMessage, fetchMessages } from '../../store/actions';
import Preloader from '../common/preloader/preloader';
import Header from './components/header/header';
import MessageList from './components/message-list/message-list';
import MessageInput from './components/message-input/message-input';


function Сhat(props) {

  const { messages, loading, user, dateLastMessage, participants } = useSelector(state => ({
    messages: state.chat.messages,
    loading: state.preloader,
    user: state.currentUser,
    dateLastMessage: state.dateLastMessage,
    participants: state.participants,
  }));


  const dispatch = useDispatch();
  const userId = user.userId;

  useEffect(() => {
    dispatch(fetchMessages());
  }, [dispatch]);



  const removeMessage = (id) => {
    dispatch(deleteMessage(id));
  };

  const addMessage = (message) => {
    dispatch(addMessage(message));
  };

  const setLikeOrDislikeMessage = (id, isLike) => {

  }

  if (loading) {
    return <Preloader />;
  }

  return (
        <>
          <Header
            messagesNumber = { messages.length }
            participants = { participants.length }
            dateLastMessage = { dateLastMessage }
          />
          <main className="main">
            <div className="container">
              <MessageList
                messages={messages}
                removeMessage={removeMessage}
                setLikeOrDislikeMessage={setLikeOrDislikeMessage} userId={userId}/>
              <MessageInput addMessage={addMessage} userId={userId}/>
            </div>
          </main>
          <footer className="footer">Copyright</footer>
        </>
  );
}

export default Сhat;
