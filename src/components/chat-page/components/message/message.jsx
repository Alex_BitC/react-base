import { formatDateToHH_MM } from '../../../../helpers/date';
import './message.css';

const Message = (props) => (
  <li className="message">
    <div className="message-user-avatar">
      <img
        src={props.message.avatar}
        alt={props.message.user}
      />
      </div>
    <div className="message-wrapper">
       <div className="message-text">{props.message.text}</div>
      <div className="message-info">
      <div className="message-user-name">{props.message.user}</div>
        <div className="message-time">
          { formatDateToHH_MM(props.message.createdAt) }
        </div>
        <button className="message-like">Like</button>
      </div>
    </div>
  </li>
);

export default Message;
