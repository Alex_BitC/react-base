import React from 'react';
const { v4: uuidv4 } = require('uuid');
import {createTimeStamp } from '../../../../helpers/date';
import './message-input.css';


class MessageInput extends React.Component {
  constructor(props) {
    super(props)
    this.state = { value: '' }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    this.setState({ value: event.target.value })
  }

  createMessage = (message) => {
    return {
      "id": uuidv4(),
      "userId": this.props.userId,
      "avatar": "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      "user": "Ruth",
      "text": message,
      "createdAt": createTimeStamp(),
      "editedAt": ""
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    const newMessage = this.createMessage(this.state.value);
    this.props.addMessage(newMessage);
    this.setState({ value: '' })
  }

  render() {
    return (
      <form className="message-input">
        <input className="message-input-text"
          type="text"
          value={this.state.value}
          placeholder="Message"
          onChange={this.handleChange}
        />
        <button className="message-input-button" onClick={this.handleSubmit}>Send</button>
      </form>
    )
  }
}

export default MessageInput;
