import { formatDateToHH_MM } from '../../../../helpers/date.js';
import './own-message.css';

const OwnMessage = (props) => {

  const handleClickEdit = (event) => {
    event.preventDefault();
    props.editMessage(props.message);
  }

  const handleClickDelete = () => {
    event.preventDefault();
    props.removeMessage(props.message.id);
  }

  return (
    <li className="own-message">
      <div className="message-text">{props.message.text}</div>
      <div className="message-info">
        <div className="message-time">
          { formatDateToHH_MM(props.message.createdAt) }
        </div>
        <button className="message-edit" onClick={handleClickEdit}>Edit</button>
        <button className="message-delete" onClick={handleClickDelete}>Delete</button>
      </div>
    </li>
  );
}

export default OwnMessage;
