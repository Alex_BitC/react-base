import { formatDateToDD_MM_YYYY_HH_MM_SS } from '../../../../helpers/date';
import './header.css';

const Header = (props) => (
<header className="header">
    <div className="container">
      <h1 className="header-title">My chat</h1>
      <div className="header-users-count">{ props.participants } participants</div>
      <div className="header-messages-count">{ props.messagesNumber } messages</div>
      <div className="header-last-message-date">
        last message at { formatDateToDD_MM_YYYY_HH_MM_SS(props.dateLastMessage) }
      </div>
    </div>
  </header>
);

export default Header;
