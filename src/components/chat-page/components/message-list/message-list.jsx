import { addDividers } from '../../../../helpers/date.js';
import Message from '../message/message';
import MessagesDivider from '../messages-divider/messages-divider';
import OwnMessage from '../own-message/own-messages';
import './message-list.css';

const MessageList = (props) => {
  const messages = props.messages;
  const userId = props.userId;
  const messagesAndDivider = addDividers(messages);

  const listItems = messagesAndDivider.map((message, index) => {
    if (typeof message === 'string') {
      return <MessagesDivider title={message} key = { index }/>
    }
    if (message.userId === userId) {
      return <OwnMessage key={ message.id } message={ message } removeMessage={props.removeMessage}/>
    }
    return <Message key={ message.id } message={ message } setLikeOrDislikeMessage={props.setLikeOrDislikeMessage}/>
  });

  return <ul className="message-list">{ listItems }</ul>
};


export default MessageList;
