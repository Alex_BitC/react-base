import './messages-divider.css';

const MessagesDivider = (props) => (
  <li className="messages-divider">
    <div className="messages-divider-line"></div>
    <div className="messages-divider-text">{props.title}</div>
    <div className="messages-divider-line"></div>
  </li>
);

export default MessagesDivider;
