import React, { useEffect } from 'react'
import Preloader from '../common/preloader/preloader';
import UserList from './components/user-list/users-list';
import {UsersFromJSON } from '../../users';
import './users-page.css';

function UsersPage(props) {
  // let url = props.url;
  const [users, setUsers] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  const [userId, setUserId] = React.useState(null);
  const [participants, setpParticipants] = React.useState([]);
  const [dateLastMessage, setDateLastMessage] = React.useState(null);

  // useEffect(() => {
  //   fetch(url, {mode: 'cors'})
  //     .then(response => response.json())
  //     .then(users => {

  //       setpParticipants(Array.from(new Set(messages.map(message => message.userId))));
  //       setDateLastMessage(messages[0].createdAt)
  //       setMessages(messages);
  //       setLoading(false);
  //     });
  // }, []);

  useEffect(() => {
    setUsers(UsersFromJSON)
    setLoading(false);

  }, []);


  const removeUser = (id) => {
    setUsers(users.filter(user => user.userId !== id));
    if (!users.length) {
      setLoading(false);
    }
  };

  const addUser = () => {
    <Navigate to={{ pathname: AppRoute.USERS_EDIT_$ID }} />
  };

  const editUser = () => {

  };

  if (loading) {
    return <Preloader />;
  }

  return (
    <>
      {users.length ? (
        <>
          <main className="main">
            <div className="container">
              <button className="button-add-user" onClick={addUser}>Add user</button>
              <UserList
                users={users}
                removeUser={removeUser}
                editUser={editUser}/>
            </div>
          </main>
        </>
      ) : loading ? null : (
        <p>No users!</p>
      )}
    </>
  );
}

export default UsersPage;
