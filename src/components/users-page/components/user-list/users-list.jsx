import UserItem from '../user-item/user-item';
import './users-list.css';

const UsersList = (props) => {
  const users = props.users;

  const listItems = users.map((user, index) => {
    return <UserItem key={ user.userId } user={ user } removeUser={props.removeUser}/>
  });

  return <ul className="users-list">{ listItems }</ul>
};


export default UsersList;
