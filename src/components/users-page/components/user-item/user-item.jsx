import './user-item.css';

const UserItem = (props) =>{
  const userId = props.user.userId;

  const handleClickEdit = (event) => {
    event.preventDefault();
    const userEdit = props.user;
    <Navigate to={{ pathname: USERS_EDIT_$ID }} />;
  }

  const handleClickDelete = (event) => {
    event.preventDefault();
    props.removeUser(userId);
  }

  return (
    <li className="user-item">
      <div className="user-info">
        {props.user.user} {props.user.eMail}
      </div>
      <button className="user-edit" onClick={handleClickEdit}>Edit</button>
      <button className="user-delete" onClick={handleClickDelete}>Delete</button>
    </li>
  );
};

export default UserItem;
