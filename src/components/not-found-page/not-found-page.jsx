import { NavLink } from "react-router-dom";
import { AppRoute } from '../common/enums/app-route';

import './style.css';

const NotFoundPage = () => (
  <section className="page-notfound">
    <h2>Page not found</h2>
    <p>Oops... page not found! </p>
    <p> Go to <NavLink to={AppRoute.ROOT}> Home </NavLink> page</p>
  </section>
);

export default NotFoundPage;
