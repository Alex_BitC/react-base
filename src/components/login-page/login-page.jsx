import React from 'react';
import { useDispatch } from 'react-redux';
import { setCurrentUser } from '../../store/actions';
import './login-page.css';

function LoginPage() {
  const [login, setLogin] = React.useState('');
  const [password, setPassword] = React.useState('');

  const dispatch = useDispatch();

  const onChangeLogin = (event) => {
    setLogin(event.target.value);
  };

  const onChangePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!login || !password) {
      return;
    }
    checkLogin(login, password);
    setPassword('');
    setLogin('');
  };

  const checkLogin = (login, password) => {
    dispatch(setCurrentUser(login, password));
  };

  return (
    <form className="login-page">
      <p>
        <label htmlFor='login-login'> Login: </label>
          <input className="login-input-text"
            id='login-login'
            type="text"
            name="login"
            placeholder="Login"
            value={login}
            onChange={onChangeLogin}/>

      </p>
      <p>
        <label htmlFor='password-password'> Password: </label>
          <input className="login-input-text"
            id='password-password'
            type="password"
            name="password"
            value={password}
            onChange={onChangePassword}/>
      </p>
      <button className="message-input-button" onClick={handleSubmit}>Send</button>
    </form>
  );
};

export default LoginPage;
