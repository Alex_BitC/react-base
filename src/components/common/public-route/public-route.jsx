import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { AppRoute } from '../enums/app-route';
// import { locationType } from 'common/prop-types/prop-types';

const PublicRoute = ({ component: Component, ...rest }) => {
  const { currentUser, admin } = useSelector(state => ({
    currentUser: state.currentUser,
    admin: state.admin
  }));

  const hasUser = Boolean(currentUser);
  const hasAdmin = Boolean(admin);

  return hasAdmin
    ? <Navigate to={{ pathname: AppRoute.USERS}} />
    : hasUser
    ? <Navigate to={{ pathname: AppRoute.ROOT}} />
    : <Component {...rest} />;
};

PublicRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
  // location: locationType
};

PublicRoute.defaultProps = {
  location: undefined
};

export default PublicRoute;
