import './preloader.css';

const Preloader = () => (
  <div className="preloader">Loading...</div>
);

export default Preloader;
