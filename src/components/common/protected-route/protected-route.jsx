import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { AppRoute } from '../enums/app-route';
// import { locationType } from 'common/prop-types/prop-types';

const ProtectedRoute = ({ component: Component, ...rest }) => {
  const { admin } = useSelector(state => ({
    admin: state.admin
  }));

  const hasAdmin = Boolean(admin);

  return hasAdmin
    ? <Component {...rest} />
    : <Navigate to={{ pathname: AppRoute.LOGIN}} />;
};

ProtectedRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
  // location: locationType
};

ProtectedRoute.defaultProps = {
  location: undefined
};

export default ProtectedRoute;
