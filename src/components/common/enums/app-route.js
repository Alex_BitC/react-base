const AppRoute = {
  ROOT: '/',
  LOGIN: '/login',
  USERS: '/users',
  USERS_EDIT_$ID: '/users/edit/:id',
  ANY: '*',
};

export { AppRoute };