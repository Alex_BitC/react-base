import { Navigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { AppRoute } from '../enums/app-route';
// import { locationType } from 'common/prop-types/prop-types';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { currentUser } = useSelector(state => ({
    currentUser: state.currentUser
  }));

  const hasUser = Boolean(currentUser);

  return hasUser
    ? <Component {...rest} />
    : <Navigate to={{ pathname: AppRoute.LOGIN}} />;
};

PrivateRoute.propTypes = {
  component: PropTypes.elementType.isRequired,
  // location: locationType
};

PrivateRoute.defaultProps = {
  location: undefined
};

export default PrivateRoute;
