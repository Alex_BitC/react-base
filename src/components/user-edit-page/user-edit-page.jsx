const { v4: uuidv4 } = require('uuid');
import { AppRoute } from '../common/enums/app-route';


function UserEditPage() {
  const [login, setLogin] = React.useState([]);
  const [password, setPassword] = React.useState(true);
  const [userName, setUserName] = React.useState(true);
  const [email, setEmail] = React.useState(true);

  const onChangeLogin = (event) => {
    setLogin(event.target.value);
  };

  const onChangePassword = (event) => {
    setPassword(event.target.value);
  };

  const onChangeUserName = (event) => {
    setUserName(event.target.value);
  };

  const onChangeEmail = (event) => {
    setEmail(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!login || !passsword || !userName || !email) {
      return;
    }
    editUser();
    setPassword('');
    setLogin('');
    setUserName('');
    setEmail('');
    <Navigate to={{pathname: AppRoute.USERS }} />


  };

  editUser = () => {
    return {
      userId: uuidv4(),
      user: userName,
      login: login,
      password: password,
      eMail: email
    }
  }


  return (
    <form className="user-edit-page">
      <p>
        <label> User name: 
          <input className="login-input-text"
            type="text"
            name="userName"
            placeholder="User name"
            value={userName}
            onChange={onChangeUserName}/>
          </label>
      </p>
      <p>
        <label> Email: 
          <input className="login-input-text"
            type="email"
            name="email"
            placeholder="Email"
            value={email}
            onChange={onChangeEmail}/>
          </label>
      </p>
      <p>
        <label> Login: 
          <input className="login-input-text"
            type="text"
            name="login"
            placeholder="Login"
            value={login}
            onChange={onChangeLogin}/>
          </label>
      </p>
      <p>
        <label> Password: 
          <input className="login-input-text"
            type="text"
            name="password"
            value={password}   
            onChange={onChangePassword}/>
          </label>
      </p>
      <button className="message-input-button" onClick={handleSubmit}>Send</button>
    </form>
  );
}

export default UserEditPage;